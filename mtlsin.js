module.exports = function(RED) {
    function mTLSinNode(config) {
        RED.nodes.createNode(this, config);
	var node = this;
	
	var crypto = require("crypto");
        var https = require("https");

	var port = config.port * 1;

	const options = {
		key: config.serverkey + '',
		cert:  config.servercert + '',
		ca: [
			config.cacert + ''
		],
		requestCert: true,
		rejectUnauthorized: true
	};

	server = https.createServer(options, function(req, res) {
		var cert = req.socket.getPeerCertificate(true).raw.toString('base64');
		cert = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----`;
		const cert_parsed = new crypto.X509Certificate(cert);
		//node.warn(cert_parsed.subject);
		//node.warn(cert);
		//node.warn(cert_parsed.fingerprint);

		var msg = {}
		msg.res = res;
		msg.req = req;
		msg.x509cert = cert_parsed;
		msg.payload = cert;
		node.send(msg);
		//res.writeHead(200);
		//res.end("OK!\n");
	})
	server.listen(port);
	
	node.on('close', function() {
	       //node.warn("MTLS-in close event");
               node.closing = true;
               server.close();
               node.log(RED._("http stopped"));
        });

    }
    RED.nodes.registerType("mtls-in", mTLSinNode);
}
