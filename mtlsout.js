module.exports = function(RED) {
    function mTLSoutNode(config) {
        RED.nodes.createNode(this, config);
	var node = this;
	
	var crypto = require("crypto");
        var https = require("https");

	var code = config.code * 1;

	node.on('input', function(msg) {
		res = msg.res;
		res.writeHead(code);
		res.end(`${msg.payload}\n`);
	}); 

    }
    RED.nodes.registerType("mtls-out", mTLSoutNode);
}
