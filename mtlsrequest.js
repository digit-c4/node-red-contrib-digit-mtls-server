module.exports = function(RED) {
    function mTLSrequestNode(config) {
        RED.nodes.createNode(this, config);
	var node = this;
	
        var https = require("https");

	var port = config.port * 1;
	var host = config.host + '';
  var path = config.path = '';
	var key = config.key + '';
	var cert = config.cert + '';
	var cacert = config.cacert + '';


	node.on('input', function(msg) {
		const options = {
			host: host,
			port: port,
			key: key,
			cert:  cert,
			ca: [
				cacert	
			],
			secureProtocol: "TLSv1_2_method",
			requestCert: true,
			rejectUnauthorized: false,
			agent: false,
			path: path,
			method: "GET",
			headers: {
				"Content-Length": Buffer.byteLength(msg.payload + '')
			}
		};

		const req = https.request(options, function(res) {
			msg.statusCode = res.statusCode;

			var rawData = "";

			res.on("data", function(data) {
				rawData += data;
			});
			res.on("end", function() {
				msg.payload = rawData;
				req.end();
				node.send(msg);
			});
		});

		req.on("socket", function(socket) {
			socket.on("secureConnect", function() {
				if (socket.authorized === false) {
					//node.warn("SOCKET AUTH FAILED");
				}
			});
			socket.setTimeout(1000);
			socket.on("timeout", function() {
				//node.warn("TLS Socket Timeout");
				req.end();
			});
		});
		req.on("error", function(err) {
			node.warn(`TLS Socket Error: ${err}`);
		});

		req.end();

	});

 }
 RED.nodes.registerType("mtls-request", mTLSrequestNode);
}
