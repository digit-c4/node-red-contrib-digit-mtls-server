# node-red-contrib-digit-mtls-server

This module adds a new Palette in Node-RED that offers mTLS functionality. The Nodes will be groupped under the `digit` category.

It contains 3 nodes: 
 - mtls-in
 - mtls-out
 - mtls-request


## mtls-in

An HTTPS server that does mutual TLS authentication.


Configuration elements:
 - Name
 - Port
 - Server Certificate
 - Server Key
 - CA Certificate 


Output: `msg.payload` will contain the client's x509 string. 


## mtls-out

A mutual HTTPS server response.


Configuration elements:
 - Name
 - StatusCode


## mtls-request

An HTTPS client that does mutual TLS authentication.


Configuration elements:
 - Name
 - Host
 - Port
